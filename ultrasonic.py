#ultrasonic.py
# Libraries
import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
#GPIO mode (BCM or BAORD)
GPIO.setmode(GPIO.BCM)

#defining GPIO inputs and outputs
TRIG=12 #setting pin for GPIO pin for TRIG
ECHO=7 #setting GPIO pin for ECHO
LIGHT=8 #setting GPIO pin for LIGHTS    
BEZO =25
GPIO.setup(TRIG,GPIO.OUT)#TRIG
GPIO.setup(ECHO,GPIO.IN)#ECHO
GPIO.setup(LIGHT,GPIO.OUT)#LIGHT
GPIO.setup(BEZO,GPIO.OUT)#LIGHT
p = GPIO.PWM(25,100)
#distance function for ultrasonic sensor
def distance():

        GPIO.output(TRIG,False)#set TRIG to low
        time.sleep(2)#TRIG goes high after set time

        GPIO.output(TRIG,True)
        time.sleep(0.00001) #TRIG goes low after set timee
        GPIO.output(TRIG,False)


        while GPIO.input(ECHO) == 0: #take in start time
                pulse_start = time.time()


        while GPIO.input(ECHO) == 1: #take in end time
                pulse_end = time.time()

        pulse_duration = pulse_end - pulse_start #find difference between times

        distance = pulse_duration *17150 #find total distance

        distance = round(distance,2) #round and divide by two, there and back

        return distance #in cm

if __name__ == '__main__':
  while True:
                dist=distance()
                print dist
                if dist<13: #if car is detected
                        print "CAR" #in cm
                        GPIO.output(LIGHT, GPIO.HIGH)
                        GPIO.output(BEZO, GPIO.HIGH)
                        p.start(100)
                        p.ChangeDutyCycle(90)
                        p.ChangeFrequency(400)
                        time.sleep(2)
                        p.stop()
                else:
                        print "NO CAR"
                        GPIO.output(LIGHT, GPIO.LOW)
                        GPIO.output(BEZO, GPIO.LOW)

                time.sleep(0.001)
